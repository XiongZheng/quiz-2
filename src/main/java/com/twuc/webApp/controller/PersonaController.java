package com.twuc.webApp.controller;

import com.twuc.webApp.domain.Persona;
import com.twuc.webApp.domain.RequestUpdateZone;
import com.twuc.webApp.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.ZoneId;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/staffs")
public class PersonaController {

    @Autowired
    private PersonaService personaService;

    private Set<String> availableZoneIds;

    @GetMapping("{id}")
    public ResponseEntity<Persona> get(@PathVariable Long id) {
        Persona persona = personaService.find(id);
        ResponseEntity<Persona> responseEntity;
        if (null == persona) {
            responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            responseEntity = ResponseEntity.status(HttpStatus.OK).body(persona);
        }
        return responseEntity;
    }

    @PostMapping
    public ResponseEntity<Void> create(@RequestBody @Valid Persona persona) {
        Persona personaById = personaService.createPersona(persona);

        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location", "/api/staffs/" + personaById.getId())
                .build();
    }

    @GetMapping
    public ResponseEntity<List<Persona>> get() {
        List<Persona> personaById = personaService.find();

        return ResponseEntity.status(HttpStatus.OK)
                .body(personaById);
    }

    @PutMapping("/{id}/timezone")
    public ResponseEntity<Void> update(@RequestBody() RequestUpdateZone zone, @PathVariable Long id) {
        ResponseEntity<Void> responseEntity;

        if (!checkZoneId(zone.getZoneId())){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        Persona persona = personaService.find(id);

        if (null == persona) {
            responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            persona.setZoneId(zone.getZoneId());
            personaService.update(persona);
            responseEntity = ResponseEntity.status(HttpStatus.OK).build();
        }
        return responseEntity;
    }

    private boolean checkZoneId(String zoneId) {

        if (null==availableZoneIds){
            availableZoneIds= ZoneId.getAvailableZoneIds();
        }
        return availableZoneIds.contains(zoneId);
    }

}
