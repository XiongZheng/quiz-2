package com.twuc.webApp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZoneId;
import java.util.Set;

@RestController
@RequestMapping("/api/timezones")
public class TimeZoneController {
    private Set<String> availableZonedIds;

    @GetMapping
    public ResponseEntity<String[]> get() {
        if (null == availableZonedIds) {
            availableZonedIds= ZoneId.getAvailableZoneIds();
        }
        return ResponseEntity.status(HttpStatus.OK).body(availableZonedIds.toArray(new String[0]));
    }
}
