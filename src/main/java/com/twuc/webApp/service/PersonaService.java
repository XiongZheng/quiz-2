package com.twuc.webApp.service;

import com.twuc.webApp.domain.Persona;
import com.twuc.webApp.repository.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonaService {
    @Autowired
    private PersonaRepository personaRepository;

    public Persona createPersona(Persona persona) {
        Persona personaSaved = personaRepository.saveAndFlush(persona);
        return personaSaved;
    }

    public Persona find(Long id) {
        Optional<Persona> personaById = personaRepository.findById(id);
        if (personaById.isPresent()){
            return personaById.get();
        }
        return null;
    }

    public List<Persona> find() {
        return personaRepository.findAll();
    }

    public void update(Persona persona){
        personaRepository.saveAndFlush(persona);
    }
}
