package com.twuc.webApp.domain;

public class RequestUpdateZone {

    private String zoneId;

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }
}
