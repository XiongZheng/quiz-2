package com.twuc.webApp;

import com.twuc.webApp.controller.PersonaController;
import com.twuc.webApp.domain.Persona;
import com.twuc.webApp.domain.RequestUpdateZone;
import com.twuc.webApp.repository.PersonaRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class PersonaControllerTest extends ApiTestBase {

    @Autowired
    PersonaRepository personaRepository;

    @Autowired
    PersonaController personaController;

    //story1
    @Test
    void should_return_status_201_when_user_create_success() throws Exception {
        Persona persona = new Persona("Rob", "Hall");

        MockHttpServletRequestBuilder mockHttpServletRequestBuilder = post("/api/staffs")
                .content(serialize(persona))
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(mockHttpServletRequestBuilder)
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/staffs/1"));
    }

    @Test
    void should_get_persona_message() {
        Persona persona = new Persona("Rob", "Hall");
        personaRepository.saveAndFlush(persona);

        ResponseEntity<Persona> responseEntity = personaController.get(persona.getId());

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("Rob", responseEntity.getBody().getFirstName());
        assertEquals("Hall", responseEntity.getBody().getLastName());
    }

    @Test
    void should_return_status_404_when_id_not_exist() {
        ResponseEntity<Persona> responseEntity = personaController.get(2019L);

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    void should_get_all_user_message() {
        //Arrange
        Persona persona = new Persona("Rob", "Hall");
        Persona persona2 = new Persona("Rob", "Hall");
        personaRepository.saveAndFlush(persona);
        personaRepository.saveAndFlush(persona2);
        List<Persona> list = Arrays.asList(persona, persona2);

        //Act
        ResponseEntity<List<Persona>> response = personaController.get();
        //Assert

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertArrayEquals(list.toArray(), response.getBody().toArray());
    }

    //story2

    @Test
    void should_return_status_200_when_update_zoneId_success() {
        Persona persona = new Persona("Rob", "Hall");
        personaRepository.saveAndFlush(persona);
        RequestUpdateZone requestUpdateZone = new RequestUpdateZone();
        requestUpdateZone.setZoneId("Asia/Chongqing");

        ResponseEntity responseEntity = personaController.update(requestUpdateZone,1L);

        assertEquals(HttpStatus.OK,responseEntity.getStatusCode());
    }

    @Test
    void should_return_status_400_when_not_provided_zoneId_or_incorrect_zoneId() {
        Persona persona = new Persona("Rob", "Hall");
        personaRepository.saveAndFlush(persona);
        RequestUpdateZone requestUpdateZone = new RequestUpdateZone();
        requestUpdateZone.setZoneId("error_zone");

        ResponseEntity responseEntity = personaController.update(requestUpdateZone, 1L);

        assertEquals(HttpStatus.BAD_REQUEST,responseEntity.getStatusCode());
    }

    @Test
    void should_return_zoneId_when_get_user_message() {
        Persona persona = new Persona("Rob", "Hall");
        personaRepository.saveAndFlush(persona);
        RequestUpdateZone requestUpdateZone = new RequestUpdateZone();
        requestUpdateZone.setZoneId("Asia/Chongqing");
        personaController.update(requestUpdateZone,1L);

        ResponseEntity<Persona> responseEntity = personaController.get(persona.getId());

        assertEquals("Asia/Chongqing",responseEntity.getBody().getZoneId());
    }
}
