Story 1 添加或获取 Rob 用户信息
AC 1：向系统中添加 Rob 用户
AC 2：获取 Rob 用户的信息
AC 3：获得所有的 Rob 用户信息

Story 2 设置 Rob 的时区信息
AC 1：为 Rob 用户设置时区信息
AC 2：在获得 Rob 用户信息时返回时区信息

Story 3 获得时区列表
AC 1：获得时区列表

Story 4 Sofia 预约 Rob 用户进行审核
AC 1：Sofia 添加预约
AC 2：获得预约列表
AC 3：失败的预约